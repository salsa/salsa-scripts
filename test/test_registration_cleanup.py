import pytest
from dataclasses import dataclass
from salsa.registration import (
    RegistrationAnalyzer,
    Action,
    Delete,
    Skip,
    Reject,
)


@dataclass
class User:
    name: str
    email: str = None


@pytest.fixture
def analyzer():
    return RegistrationAnalyzer(None)


class TestAction:
    def test_action_name(self):
        assert str(Delete(None, None)) == "delete"
        assert str(Reject(None, None)) == "reject"
        assert str(Skip(None, None)) == "skip"

    def test_by_name(self):
        assert type(Action.by_name("delete", None, None)) is Delete


class TestRegistrationCleanup:
    def test_firstname_eq_lastname(self, analyzer):
        user = User(name='Foobar Foobar')
        action = analyzer.process(user)
        assert type(action) is Delete

    def test_non_suspicious_user(self, analyzer):
        user = User(name="Antonio Terceiro")
        action = analyzer.process(user)
        assert type(action) is Skip

    @pytest.mark.parametrize(
        "email",
        [
            "xyz123@gmail.com",
            "littlething666@yahoo.com",
        ]
    )
    def test_probable_spammer(self, analyzer, email):
        user = User(name="Something Suitable", email=email)
        action = analyzer.process(user)
        assert type(action) is Reject

    @pytest.mark.parametrize(
        "email",
        [
            "someone@gufum.com",
            "someone@mailinator.com",
            "someone@pimmel.top",
        ]
    )
    def test_temp_mailbox_domain(self, analyzer, email):
        user = User(name="Something Suitable", email=email)
        action = analyzer.process(user)
        assert type(action) is Reject
