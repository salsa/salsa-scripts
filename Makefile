.PHONY: test

all: test style

test:
	python3 -m pytest

style:
	flake8
