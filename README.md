Sample configuration file (`~/.python-gitlab.cfg`):

```toml
[global]
default = salsa
ssl_verify = true
timeout = 5

[salsa]
url = https://salsa.debian.org
private_token = ******************
api_version = 4
```
