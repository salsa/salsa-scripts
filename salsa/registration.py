import logging
import re
import sys
from typing import cast, Optional, Union
import gitlab
import gitlab.v4.objects


logger = logging.getLogger()


class UserManager(gitlab.v4.objects.users.UserManager):
    def reject(self, id: Union[int, str]):
        path = f"/users/{id}/reject"
        server_data = cast(Optional[bool], self.gitlab.http_post(path))
        return server_data

    def approve(self, id: Union[int, str]):
        path = f"/users/{id}/approve"
        server_data = cast(Optional[bool], self.gitlab.http_post(path))
        return server_data


class Gitlab(gitlab.Gitlab):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.users = UserManager(self)

    def users(self):
        return


class Action:
    def __init__(self, connection, user):
        self.connection = connection
        self.user = user

    def __str__(self):
        return type(self).__name__.lower()

    def perform(self):
        return NotImplementedError()

    @classmethod
    def by_name(cls, name, connection, user):
        # FIXME check that name matches a subclass of action
        cls_name = name.capitalize()
        action_class = getattr(sys.modules[__name__], cls_name, None)
        if not action_class:
            action_class = Skip
            logger.warning(
                f"Invalid action \"{name}\" for user {user.username}; skipping"
            )
        return action_class(connection, user)


class Skip(Action):
    def perform(self):
        pass


class Delete(Action):
    def perform(self):
        self.connection.users.delete(id=self.user.id)


class Reject(Action):
    def perform(self):
        self.connection.users.reject(id=self.user.id)


class Approve(Action):
    def perform(self):
        self.connection.users.approve(id=self.user.id)


class RegistrationAnalyzer:
    def __init__(self, connection):
        self.connection = connection

    spammer_domains = [
        "gmail.com",
        "yahoo.com",
        "126.com",
        "163.com",
    ]

    temp_mailboxes_domains = [
        "gufum.com",
        "pimmel.top",
        "mailinator.com"
    ]

    bogus_domains = [
        "drown.college",
        "regularemail.shop",
        "wineonline.one",
        "greatemails.online",
        "quirkyemails.fun",
        "funemails.shop",
        "quantumnest.lat",
        "thunderecho.store",
        "itchydog.online",
        "skywardhub.online",
        "snterra.com",
        "zebarrier.com",
        "schemeza.com",
        "ctcables.com",
        "medicalze.com",
        "bcaccept.com",
        "porkmiss.com",
        "repee.com",
        "xemedium.com",
        "wipeso.com",
        "safekiosk.com",
        "coylevarland.com",
        "sheepxme.com",
        "risekka.com",
        "whomio.com",
        "themeqa.com",
        "slidegs.com",
        "newenglandcoinco.com",
        "transatusa.com",
        "aliyun.com",
        "brolady.com",
        "callsbo.com",
        "cheapsuperjerseysfans.com",
        "duxarea.com",
        "findabridge.com",
        "hipergen.com",
        "hollytierney.com",
        "icacica.com",
        "liontiny.com",
        "mailmanila.com",
        "mzable.com",
        "prenball.com",
        "priorkt.com",
        "sensewa.com",
        "tinyvia.com",
        "uniquemoz.com",
        "frupia.com",
        "uikyam.com",
        "hoihhi.com",
        "ioaaau.com",
        "courrl.com",
        "sbaron.com",
        "spacemail.click",
        "dontstress.online",
        "fruitingbodymushrooms.online",
        "topvics.com",
        "sosewdi.com",
        "twicebro.com",
        "email.*.(?:space|website|online|store)",
        ".*.spotifygold.ir",
        ".*.herbhouse.ir",
        ".*.nex4.ir",
        ".*.next4.ir",
        ".*.jenniferlawrence.uk",
        ".*.gemmasmith.co.uk",
        ".*.sarahconner.co.uk",
        ".*.marymarshall.co.uk",
        ".*.claychoen.top",
        ".*.proofcatch.net",
        ".*.thedailygood.ir",
        ".*.letslovethoughts.ir",
        ".*.cosmicbridge.site",
        ".*.thebetter.online",
        ".*.tradingcogroup.ir",
    ]

    def process(self, user):
        parts = user.name.split()
        if len(parts) == 2 and parts[0] == parts[1]:
            return Delete(self.connection, user)
        if user.email:
            if self.spammer_email(user.email):
                return Reject(self.connection, user)
            elif self.match_domain(user.email, self.temp_mailboxes_domains):
                return Reject(self.connection, user)
            elif self.match_domain(user.email, self.bogus_domains):
                return Delete(self.connection, user)

        return Skip(self.connection, user)

    def spammer_email(self, email):
        domains = self.spammer_domains
        matches = [True for d in domains if re.match(".*[0-9]+@" + d, email)]
        return len(matches) > 0

    def match_domain(self, email, domains):
        matches = [True for d in domains if re.match(".*@" + d, email)]
        return len(matches) > 0
